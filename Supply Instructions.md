## Instructions to setup Supply for automating Google Play Console 

1. Open the **[Google Cloud Console](https://console.cloud.google.com/)**
2. Select your project
3. Search **Google Play Android Developer API** and select it.
4. Click **Enable**
5. Open [Google Play Console](https://play.google.com/apps/publish)
6. Click **Settings → Developer Account → API access**
7. Select **Link existing project** and Choose same project as selected in Step 2 and click **Link Project**. 
8. Under **Service accounts** click the **Create new service account** button
9. Follow the **[Google Cloud Console](https://console.cloud.google.com/)** link in the dialog, which opens a new tab/window:
    1. Click the **CREATE SERVICE ACCOUNT** button at the top of the Google Developers Console
    2. Provide a Service account name, description and click **Create**
    3. Click Select a **Role**, then find and select **Service Account User**, and proceed.
    4. Click the **Done** button
    5. Click on the Actions vertical **three-dot icon** of the service account you just created
    6. Select **Manage keys** on the menu
    7. Click **ADD KEY**  →  **Create New Key**
    8. Make sure **JSON** is selected as the Key type, and click **CREATE**
    9. Save the file on your computer when prompted and remember where it was saved to
10. Back on the Google Play Console, click **DONE** to close the dialog
11. Click on **Grant access** for the newly added service account at the bottom of the screen
12. Choose the permissions you'd like this account to have. We recommend **Admin (all permissions)**, but you may want to manually select all checkboxes and leave out some of the **Releases** permissions such as **Release to production.**
13. Click **Invite user** to finish.
