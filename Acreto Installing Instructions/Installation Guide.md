# Overview
This guide will help you to configure Acreto Secure connection on your Android device with the help of the Acreto Android app.

##### 1. Download the configuration file
1. Open https://wedge.acreto.net in your favorite browser. 
2. Add a thing named laptop on Acreto Ecosystem - [check how to do it](https://kb.acreto.net/how-to/quickstart/connect-first-thing/#add-thing-to-acreto-wedge)

![Add thing](https://kb.acreto.net/how-to/connect/acreto-connect/img/openvpn-download-android-ovpn-1.jpg)

3. Open the **laptop** thing details

![Edit Thing](https://kb.acreto.net/how-to/connect/acreto-connect/img/openvpn-download-android-ovpn-2.jpg)

4. Click on **Download OpenVPN config file** to save the configuration **.ovpn** file.

![Download OVPN](https://kb.acreto.net/how-to/connect/acreto-connect/img/openvpn-download-android-ovpn-3.jpg)

##### 2. Create connection
1. Go to the Google Play Store 

![Open Play Store](https://kb.acreto.net/how-to/connect/acreto-connect/img/openvpn-config-android_01.jpg)

2. Search for the Acreto Connect Client application
3. Install the Acreto Connect Client application

![Install Play Store](Files/install_play.png)

4. Once the application is installed, launch it from your home screen or menu.

![Launch App](Files/splash.jpg)

5. Tap on Next icon until you see **Get Started** text. Tap on it.

![Next](Files/intro_1.jpg)

6. Tap **Allow** to grant permissions for reading your files.

![Allow Permissions](Files/permission.jpg)

8. Select **File** option to select **.ovpn** file

![Select File](Files/select_files.jpg)

9. Select your downloaded **Acreto Configuration File** ending with **.ovpn**

![Select configuration file](Files/select_from_explorer.jpg)

10. Now, if configuration file require **Username** and **Password** to connect then enter these details and click **Save**

![Save Credentials](Files/save_credentials.jpg)

11. Turn toggle on for initiating connection.

![Toggle to start](Files/toggle_to_start.jpg)

11. When asked for permissions. Click on **OK**

![Allow VPN](https://kb.acreto.net/how-to/connect/acreto-connect/img/openvpn-config-android_10_connection.request.jpg)

12. The connection is successful. You can see that toggle has been turned on for established connection.

![Connected](Files/toggle_turned_on.jpg)

13. To disconnect, turn toggle off.

![Disconnect](Files/toggle_disconnect.jpg)
