##### System Level:
In android we can't install Certificates as System trusted certificate because for that the android device needs to have root access as mentioned [here](http://wiki.cacert.org/FAQ/ImportRootCert#Android_Phones_.26_Tablets)

![Root certificate](Certificate Installation/system_certificate.jpg)

##### User Level:
To install CA certificate as User trusted certificate into android device user have to follow these manual steps [mentioned here as well](https://support.google.com/pixelphone/answer/2844832?hl=en):
1. Open your phone's Settings app.
2. Tap Security And then Advanced And then Encryption & credentials.
3. Under "Credential storage," tap Install a certificate And then Wi-Fi certificate.
4. In the top left, tap Menu .
5. Under "Open from," tap where you saved the certificate.
6. Tap the file.
7. If needed, enter the key store password. Tap OK.
8. Enter a name for the certificate.
9. Tap OK.

Also, user trusted certificate after installation will show warning in Notification panel titled "Network may be monitered".

![Network Monitored](Certificate Installation/network_monitored.jpg)

In Android 11, the certificate installer now checks who asked to install the certificate as mentioned [here in NOTE](https://developer.android.com/work/versions/android-11#other). 

![android 11 note](Certificate Installation/android_11.jpg)

If it was launched by anybody other than the system's settings application, the certificate install is refused with an obscure alert message:

![Root warning](https://i.stack.imgur.com/J4ZHY.png)

So in Android 11 any other app cannot take user from their app inside settings to install app, Users have to manually open settings app inside phone.

##### App Level:
To install CA to app level certificates we can follow [this](https://developer.android.com/training/articles/security-config#TrustingAdditionalCas) guide but these will not show in settings -> security -> Encryption & credentials -> trusted credentials
